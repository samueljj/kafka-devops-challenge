#!/bin/bash
kafka_url="https://dlcdn.apache.org/kafka/3.0.0/kafka_2.13-3.0.0.tgz"
kafka_directory="./kafka"
bootstrap="kafka1:11091,kafka2:11092"
admin_config="./config/client-ssl.properties"
hosts_kafka=$(grep -E -- 'kafka1|kafka2' /etc/hosts)

set -euo pipefail

: '
# This main function calls docker compose, waits for the Broker and Zookeeper to come up and
then creates the ACLs and the topic FeelTheFlow.
'
main() {
  echo "Running docker-compose up"
  docker-compose up -d
  echo "Waiting for cluster to be available"
  sleep 15s
  echo "Grabbing Kafka if it does not exist"
  if [[ ! -d "$kafka_directory" ]]; then
    wget -qO- "$kafka_url" | tar xzf -
    mv kafka_2.13-3.0.0 kafka
  fi
  echo "Updating hosts file"
  if [[ -z "$hosts_kafka" ]]; then
    sudo echo $'127.0.0.1 kafka1 localhost\n127.0.0.1 kafka2 localhost' > /etc/hosts
  fi
  echo "Creating topic FeelTheFlow"
  ./kafka/bin/kafka-topics.sh --bootstrap-server "$bootstrap" --command-config $admin_config \
  --topic "FeelTheFlow" --create --partitions 1 --replication-factor 2
  echo "Applying ACLs on topic"
  ./kafka/bin/kafka-acls.sh --bootstrap-server "$bootstrap" --command-config $admin_config \
   --add --allow-principal "User:bike" --operation Write --topic FeelTheFlow
  ./kafka/bin/kafka-acls.sh --bootstrap-server "$bootstrap" --command-config $admin_config \
   --add --allow-principal "User:client" --operation Read --topic FeelTheFlow --group=*
}

main "$@"








