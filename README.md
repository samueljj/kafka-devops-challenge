# devops-challenge

## Introduction

This is the main repository for the Devops Challenge from me (Sam Jennings) for the Bosch DevOps position. I've chosen
the Kafka setup for the challenge.

## Required Packages

* Docker
* Docker-compose
* Keytool


## Directory Structure 
Please note that the JKS files here are added to the gitignore and must be generated on the client machine. The certificates
should be in the directories as shown below. There should be 4 certificates in total
* CN=zookeeper - for zookeeper 
* CN=localhost - for the broker
* CN=admin - for the admin certificate
* CN=bike - for the producer 
* CN=client - for the consumer

```
.
├── certs
│   ├── ca-key
│   ├── kafka1.keystore.jks
│   ├── kafka2.keystore.jks
│   ├── kafka-admin.keystore.jks
│   ├── kafka-localhost.keystore.jks
│   ├── kafka-secret.txt
│   ├── kafka.truststore.jks
│   └── zookeeper.keystore.jks
├── config
│   └── client-ssl.properties
├── consumer
│   ├── consumer.properties
│   ├── consumer.sh
│   ├── kafka-client.keystore.jks
│   ├── kafka.truststore.jks
│   └── test.txt
├── docker-compose.yml
├── kafka-docker-setup.sh
├── producer
│   ├── kafka-bike.keystore.jks
│   ├── kafka.truststore.jks
│   ├── producer.properties
│   └── producer.sh
└── README.md
```

## Architecture Setup

The setup is -

* One Zookeeper node
* Two Broker node

The broker uses ```kafka.security.authorizer.AclAuthorizer```
which controls the ACLs on the topics. 

There is a wrapper shell script named ```kafka-docker-setup.sh``` which launches
Zookeeper and the broker and waits for them to come up. After 15s the script downloads Kafka and unzips it. Once the 
Kafka folder is there the script calls ```kafka-topics.sh``` and ```kafka-acls.sh``` which creates the topic FeelTheFlow and
applies the correct ACLs. 

After the script exits, the producer script and the consumer script can be run to prove that the connection works.

## Caveats 

There are a number of things assumed during the challenge that aren't best practice and were done in the interest of time.

* Passwords are available in the repo in plaintext.

In a normal production or even a staging/dev setup, something like AWS Secrets Manager or Vault to store the secrets 
and keys would be used. I thought that setting something like that would be a bit out of scope.

* Portability 

Unfortunately, I have no other computers to test this on aside from my local machine running Fedora Linux, so there might 
be some issues regarding running it on other machines.

## TO-DO
If I get more time before the interview I plan to implement the following changes. I don't these would be the next changes
I would make to my setup. 

* Multi-Broker Setup - ✓
* Encryption between Zookeeper -> Kafka brokers - ✓
* Error Handling on the Bash Script.


